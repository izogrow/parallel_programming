﻿//#define MATRIX_DIM 3
//#define MATRIX_DIM 32
//#define MATRIX_DIM 64
//#define MATRIX_DIM 128
//#define MATRIX_DIM 512
#define MATRIX_DIM 630
//#define MATRIX_DIM 1023
//#define MATRIX_DIM 1024
//#define MATRIX_DIM 1024
//#define MATRIX_DIM 2048
//#define MATRIX_DIM 4196
//#define MATRIX_DIM 10000

#define STARTS_COUNT 1
#define IS_ENABLE_MATRIX_PRINTING false
//#define IS_ENABLE_MATRIX_PRINTING true

#define IDX2C(i,j,ld) (((j)*(ld))+(i))

#pragma once
#include <iostream>
#include <chrono> 
#include <cstdlib>
#include <curand.h>
#include <cublas_v2.h>

using namespace std::chrono;
using namespace std;

void print_matrix_np(float** A, const int nr_rows_A, const int nr_cols_A) {
    for (int i = 0; i < nr_rows_A; i++) {
        for (int j = 0; j < nr_cols_A; j++) {
            cout << A[i][j] << ' ';
        }
        cout << endl;
    }
}

double non_parallel() {
    // Allocate 3 arrays on CPU
    int nr_rows_A, nr_cols_A, nr_rows_B, nr_cols_B, nr_rows_C, nr_cols_C;
    // for simplicity we are going to use square arrays
    nr_rows_A = nr_cols_A = nr_rows_B = nr_cols_B = nr_rows_C = nr_cols_C = MATRIX_DIM;

    float **h_A = new float*[MATRIX_DIM];
    float **h_B = new float*[MATRIX_DIM];
    float **h_C = new float*[MATRIX_DIM];

    for (int i = 0; i < MATRIX_DIM; ++i) {
        h_A[i] = new float[MATRIX_DIM];
        h_B[i] = new float[MATRIX_DIM];
        h_C[i] = new float[MATRIX_DIM];
    }

    //float* h_A = (float*)malloc(nr_rows_A * nr_cols_A * sizeof(float));
    //float* h_B = (float*)malloc(nr_rows_B * nr_cols_B * sizeof(float));
    //float* h_C = (float*)malloc(nr_rows_C * nr_cols_C * sizeof(float));


    srand(0);
    for (int i = 0; i < MATRIX_DIM; ++i) {	//заполнение массива 
        for (int j = 0; j < MATRIX_DIM; ++j) {
            h_A[i][j] = (float)rand() / RAND_MAX;
            h_B[i][j] = (float)rand() / RAND_MAX;
        }
    }

    auto start = high_resolution_clock::now();

    for (int i = 0; i < MATRIX_DIM; ++i) {
        for (int j = 0; j < MATRIX_DIM; ++j) {
            for (int inner = 0; inner < MATRIX_DIM; ++inner) {
                h_C[i][j] += float(h_A[i][inner] * h_B[inner][j]);
            }
        }
    }

    auto stop = high_resolution_clock::now();

    if (IS_ENABLE_MATRIX_PRINTING) {
        cout << endl;
        cout << "Non-parallel execution:" << endl;
        cout << "A=" << endl;
        print_matrix_np(h_A, nr_rows_A, nr_cols_A);
        cout << "B=" << endl;
        print_matrix_np(h_B, nr_rows_A, nr_cols_A);
        cout << "C=" << endl;
        print_matrix_np(h_C, nr_rows_A, nr_cols_A);
    }
    

 
    auto duration = duration_cast<nanoseconds>(stop - start);
    //cout << "time spent non-parallel executing " << duration.count() << " nanoseconds" << endl;
    cout.precision(9);
    double duration_in_seconds = double(duration.count() * 1e-9);
    cout << fixed << "time spent non-parallel executing " << duration_in_seconds << " seconds" << endl;
    //printf("\ntime spent non-parallel executing %.10f seconds\n", (float)duration.count());

    delete h_A;
    delete h_B;
    delete h_C;

    return duration_in_seconds;
}

// Fill the array A(nr_rows_A, nr_cols_A) with random numbers on GPU
void GPU_fill_rand(float* A, int nr_rows_A, int nr_cols_A) {
    // Create a pseudo-random number generator
    curandGenerator_t prng;
    curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);
    // Set the seed for the random number generator using the system clock
    curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());

    // Fill the array with random numbers on the device
    curandGenerateUniform(prng, A, nr_rows_A * nr_cols_A);
}

// Multiply the arrays A and B on GPU and save the result in C
// C(m,n) = A(m,k) * B(k,n)
double gpu_blas_mmul(const float* A, const float* B, float* C, const int m, const int k, const int n) {
    int lda = m, ldb = k, ldc = m;
    const float alf = 1;
    const float bet = 0;
    const float* alpha = &alf;
    const float* beta = &bet;

    auto start = high_resolution_clock::now();

    // Create a handle for CUBLAS
    cublasHandle_t handle;
    cublasCreate(&handle);

    // Do the actual multiplication
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);

    // Destroy the handle
    cublasDestroy(handle);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<nanoseconds>(stop - start);
    double duration_in_seconds = double(duration.count() * 1e-9);

    return duration_in_seconds;
}

void print_matrix(const float* A, const int nr_rows_A, const int nr_cols_A) {
    for (int i = 0; i < nr_rows_A; i++) {
        for (int j = 0; j < nr_cols_A; j++) {
            cout << A[IDX2C(i, j, nr_rows_A)] << ' ';
        }
        cout << endl;
    }
}

double parallel() {

    //GPU_fill_rand() - Функция случайной генерации матрицы
    //gpu_blas_mmul() - Функция умножения матриц
    //print_matrix() - Функция вывода матрицы

    // Allocate 3 arrays on CPU
    int nr_rows_A, nr_cols_A, nr_rows_B, nr_cols_B, nr_rows_C, nr_cols_C;
    // for simplicity we are going to use square arrays
    nr_rows_A = nr_cols_A = nr_rows_B = nr_cols_B = nr_rows_C = nr_cols_C = MATRIX_DIM;
    float* h_A = (float*)malloc(nr_rows_A * nr_cols_A * sizeof(float));
    float* h_B = (float*)malloc(nr_rows_B * nr_cols_B * sizeof(float));
    float* h_C = (float*)malloc(nr_rows_C * nr_cols_C * sizeof(float));
    // Allocate 3 arrays on GPU
    float* d_A, * d_B, * d_C;
    cudaMalloc(&d_A, nr_rows_A * nr_cols_A * sizeof(float));
    cudaMalloc(&d_B, nr_rows_B * nr_cols_B * sizeof(float));
    cudaMalloc(&d_C, nr_rows_C * nr_cols_C * sizeof(float));
    // Fill the arrays A and B on GPU with random numbers
    GPU_fill_rand(d_A, nr_rows_A, nr_cols_A);
    GPU_fill_rand(d_B, nr_rows_B, nr_cols_B);
    // Optionally we can copy the data back on CPU and print the arrays
    cudaMemcpy(h_A, d_A, nr_rows_A * nr_cols_A * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_B, d_B, nr_rows_B * nr_cols_B * sizeof(float), cudaMemcpyDeviceToHost);

    if (IS_ENABLE_MATRIX_PRINTING) {
        cout << "Parallel execution:" << endl;
        std::cout << "A =" << std::endl;
        print_matrix(h_A, nr_rows_A, nr_cols_A);
        std::cout << "B =" << std::endl;
        print_matrix(h_B, nr_rows_B, nr_cols_B);
    }

    // Multiply A and B on GPU
    double duration_in_seconds = gpu_blas_mmul(d_A, d_B, d_C, nr_rows_A, nr_cols_A, nr_cols_B);
    

    // Copy (and print) the result on host memory
    cudaMemcpy(h_C, d_C, nr_rows_C * nr_cols_C * sizeof(float), cudaMemcpyDeviceToHost);

    if (IS_ENABLE_MATRIX_PRINTING) {
        std::cout << "C =" << std::endl;
        print_matrix(h_C, nr_rows_C, nr_cols_C);
    }

    //Free GPU memory
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    // Free CPU memory
    free(h_A);
    free(h_B);
    free(h_C);

    cout << fixed << "time spent parallel executing " << duration_in_seconds << " seconds" << endl;

    return duration_in_seconds;
}

int main() {
    int starts_count = STARTS_COUNT;
    double* parallel_time = new double[starts_count];
    double* non_parallel_time = new double[starts_count];
    float parallel_time_sum = 0;
    float non_parallel_time_sum = 0;
    for (int i = 0; i < starts_count; ++i) {
        parallel_time[i] = parallel();
        non_parallel_time[i] = non_parallel();
        parallel_time_sum += parallel_time[i];
        non_parallel_time_sum += non_parallel_time[i];
    }
    double average_parallel_time = parallel_time_sum / starts_count;
    double average_non_parallel_time = non_parallel_time_sum / starts_count;
    double parallel_boost = average_non_parallel_time / average_parallel_time;
    cout << "==================================================================" << endl;
    cout << "Results of " << starts_count << " starts with MATRIX_DIM = " << MATRIX_DIM << ":" << endl;
    cout << "Average parallel excecution time:     " << average_parallel_time << endl;
    cout << "Average non parallel excecution time: " << average_non_parallel_time << endl;
    cout << "Parallel boost: " << parallel_boost << endl;

    delete parallel_time;
    delete non_parallel_time;
    
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
