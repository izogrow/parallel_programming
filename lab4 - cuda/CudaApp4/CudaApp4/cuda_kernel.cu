﻿#pragma once
#include "cuda_runtime.h"
//#include "./cuda_kernel.cuh"

__global__ void addVectors(float* A, float* B, float* C, int arraySize) {
    // Get thread ID.
    int threadID = blockDim.x * blockIdx.x + threadIdx.x;

    // Check if thread is within array bounds.
    if (threadID < arraySize) {
        // Add a and b.
        C[threadID] = A[threadID] + B[threadID];
    }
}

__global__ void addVectorsSimple(float* A, float* B, float* C, int arraySize) {
    // Get thread ID.
    int threadID = blockDim.x * blockIdx.x + threadIdx.x;
  
    C[threadID] = A[threadID] + B[threadID];
    
}

#include "Header.h"