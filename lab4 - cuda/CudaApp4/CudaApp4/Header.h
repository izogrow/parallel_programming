#define NMAX 1024 
//#define NMAX 2048 
//#define NMAX 4096 
//#define NMAX 8192
//#define NMAX 16384
//#define NMAX 32768
//#define NMAX 65536
//#define NMAX 131072
//#define NMAX 1024001
#define THREADS_PER_BLOCK 512
//#define THREADS_PER_BLOCK 1024
#define STARTS_COUNT 12

#pragma once
#include <iostream>
//#include "./cuda_kernel.cuh"
#include <cuda_runtime.h>
//#include "Header.h"
#include <chrono> 

using namespace std::chrono;
using namespace std;

double non_parallel() {
    //int NMAX = n;
    int i;
    //double a[NMAX] = { 1, 2, 3 };
    float* a = new float[NMAX];
    float* b = new float[NMAX];
    float* c = new float[NMAX];

    srand(0);
    for (int i = 0; i < NMAX; ++i) {	//заполнение массива 
        //a[i] = rand() % 14;
        //cout << a[i] << endl;
        //a[i] = 1.0f;
       // b[i] = 1.0f;
        a[i] = (float)rand() / RAND_MAX;
        b[i] = (float)rand() / RAND_MAX;
    }

    auto start = high_resolution_clock::now();

    for (i = 0; i < NMAX; i++) {
        c[i] = a[i] + b[i];
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<nanoseconds>(stop - start);
    //cout << "time spent non-parallel executing " << duration.count() << " nanoseconds" << endl;
    cout.precision(9);
    double duration_in_seconds = double(duration.count() * 1e-9);
    cout << fixed << "time spent non-parallel executing " << duration_in_seconds << " seconds" << endl;
    //printf("\ntime spent non-parallel executing %.10f seconds\n", (float)duration.count());
    return duration_in_seconds;
}

double parallel() {
    // Error code to check return values for CUDA calls
    cudaError_t err = cudaSuccess;

    // Print the vector length to be used, and compute its size
    int numElements = NMAX;
    size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);

    // Allocate the host input vector A
    float* h_A = (float*)malloc(size);

    // Allocate the host input vector B
    float* h_B = (float*)malloc(size);

    // Allocate the host output vector C
    float* h_C = (float*)malloc(size);

    // Verify that allocations succeeded
    if (h_A == NULL || h_B == NULL || h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand() / (float)RAND_MAX;
        h_B[i] = rand() / (float)RAND_MAX;
    }

    // Allocate the device input vector A
    float* d_A = NULL;
    err = cudaMalloc((void**)&d_A, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vector B
    float* d_B = NULL;
    err = cudaMalloc((void**)&d_B, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Allocate the device output vector C
    float* d_C = NULL;
    err = cudaMalloc((void**)&d_C, size);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to allocate device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    printf("Copy input data from the host memory to the CUDA device\n");
    err = cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector B from host to device (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Создание обработчиков событий
    cudaEvent_t start, stop;
    float gpuTime = 0.0f;
    err = cudaEventCreate(&start);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Cannot create CUDA start event: %s\n",
            cudaGetErrorString(err));
        return 0;
    }

    err = cudaEventCreate(&stop);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Cannot create CUDA end event: %s\n",
            cudaGetErrorString(err));
        return 0;
    }


    // Launch the Vector Add CUDA Kernel
    int threadsPerBlock = THREADS_PER_BLOCK;
    int blocksPerGrid = (numElements + threadsPerBlock - 1) / threadsPerBlock;
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);

    // Установка точки старта
    err = cudaEventRecord(start, 0);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Cannot record CUDA event: %s\n",
            cudaGetErrorString(err));
        return 0;
    }

    addVectors << <blocksPerGrid, threadsPerBlock >> > (d_A, d_B, d_C, numElements);
    // addVectorsSimple << <blocksPerGrid, threadsPerBlock >> > (d_A, d_B, d_C, numElements);


    err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch vectorAdd kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Синхронизация устройств
    //err = cudaDeviceSynchronize();
    //if (err != cudaSuccess)
    //{
    //    fprintf(stderr, "Cannot synchronize CUDA kernel: %s\n",
    //        cudaGetErrorString(err));
    //    return 0;
    //}


    // Установка точки окончания
    err = cudaEventRecord(stop, 0);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Cannot copy c array from device to host: %s\n",
            cudaGetErrorString(err));
        return 0;
    }

    // Copy the device result vector in device memory to the host result vector
    // in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    err = cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy vector C from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification failed at element %d!\n", i);
            exit(EXIT_FAILURE);
        }
    }

    printf("Test PASSED\n");

    // Расчет времени
    err = cudaEventElapsedTime(&gpuTime, start, stop);
    float duration_in_seconds = gpuTime / 1000;
    printf("time spent CUDA executing %s: %.9f seconds\n", "kernel", duration_in_seconds);
    
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // Free device global memory
    err = cudaFree(d_A);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_B);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    err = cudaFree(d_C);

    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to free device vector C (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    // Free host memory
    free(h_A);
    free(h_B);
    free(h_C);

   // printf("Parallel: Done\n");
    return duration_in_seconds;
}

int main() {
    int starts_count = STARTS_COUNT;
    double* parallel_time = new double[starts_count];
    double* non_parallel_time = new double[starts_count];
    double parallel_time_sum;
    double non_parallel_time_sum;
    for (int i = 0; i < starts_count; ++i) {
        parallel_time[i] = parallel();
        non_parallel_time[i] = non_parallel();
        parallel_time_sum += parallel_time[i];
        non_parallel_time_sum += non_parallel_time[i];
    }
    double average_parallel_time = parallel_time_sum / starts_count;
    double average_non_parallel_time = non_parallel_time_sum / starts_count;
    double parallel_boost = average_non_parallel_time / average_parallel_time;
    cout << "==================================================================" << endl;
    cout << "Results of " << starts_count << " starts with THREADS_PER_BLOCK = " << THREADS_PER_BLOCK << " and n = "<< NMAX << ":" << endl;
    cout << "Average parallel excecution time:     " << average_parallel_time << endl;
    cout << "Average non parallel excecution time: " << average_non_parallel_time << endl;
    cout << "Parallel boost: " << parallel_boost << endl;
    
    delete parallel_time;
    delete non_parallel_time;

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
