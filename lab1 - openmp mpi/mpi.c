#include "mpi.h"
#include "stdio.h"
#include <time.h>
#include <math.h>
#include <omp.h>

int main(int argc, char* argv[])
{
	clock_t t;
	t = clock();

	int rank, ranksize, i; 
	MPI_Init(&argc, &argv);// 
	//���������� ���� ����� � ������:
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); //
	//���������� ������ ������:
	MPI_Comm_size(MPI_COMM_WORLD, &ranksize);//
	printf("Hello world from process %d from total number of %d\n", rank, ranksize);
	MPI_Finalize();//

	t = clock() - t;
	printf ("It took me %d clicks (%f seconds).\n", 
          (int)t, ((double)t)/CLOCKS_PER_SEC);
	return 0;
}
