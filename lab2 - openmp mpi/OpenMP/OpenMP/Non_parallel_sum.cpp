#include <omp.h>
#define CHUNK 100
#define NMAX 1440000
//#define NMAX 16
#include "stdio.h"
#include <cstdlib>
#include <iostream>

using namespace std;

double non_parallel_t() {
	omp_set_num_threads(2);
	int i;
	//double a[NMAX] = { 1, 2, 3 };
	int* a = new int[NMAX];

	srand(0);
	for (int i = 0; i < NMAX; ++i) {	//заполнение массива 
		a[i] = rand() % 14;

	}
	int sum = 0;

	double st_time, end_time;
	st_time = omp_get_wtime();

	for (i = 0; i < NMAX; i++) {
		sum + a[i];			// Q = 14
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum = sum + a[i];
	}
	end_time = omp_get_wtime();
	end_time = end_time - st_time;
	printf("\nTotal Sum = %10d", sum);
	printf("\nTIME OF WORK IS %f \n", end_time);
	return end_time;
}
