#include <math.h>
#include <omp.h>
#include <time.h>
#include <stdlib.h>
#include <locale.h>
#include <stdio.h>


int hello()
{
	clock_t t;
	t = clock();

	omp_set_num_threads(2);

	int nTheads, theadNum;
#pragma omp parallel  private(nTheads, theadNum)
	{
		nTheads = omp_get_num_threads();
		theadNum = omp_get_thread_num();
		printf("OpenMP thread ?%d from %d threads \n", theadNum, nTheads);
	}

	t = clock() - t;
	printf("It took me %d clicks (%f seconds).\n",
		(int)t, ((double)t) / CLOCKS_PER_SEC);
	return 0;
}
