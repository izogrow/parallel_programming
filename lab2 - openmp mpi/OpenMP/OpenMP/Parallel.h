#pragma once

double atomic_t();
double critical_t();
double reduction_t();
double non_parallel_t();
int hello();