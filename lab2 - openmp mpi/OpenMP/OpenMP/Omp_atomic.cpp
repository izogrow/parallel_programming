#include <omp.h>
#define CHUNK 100
#define NMAX 1440000
// #define NMAX 2
#include "stdio.h"
#include <cstdlib>
#include <iostream>

using namespace std;

double atomic_t() {
	omp_set_num_threads(2);
	int i;
	int* a = new int[NMAX];

	srand(0);
	for (int i = 0; i < NMAX; ++i) {	//заполнение массива 
		a[i] = rand() % 14;
	}
	int sum = 0;

	double st_time, end_time;
	st_time = omp_get_wtime();
	
#pragma omp parallel for shared(a) private(i)
	for (i = 0; i < NMAX; i++) {
		sum + a[i];			// Q = 14
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
		sum + a[i];
#pragma omp atomic update 
		sum = sum + a[i];
	}
	end_time = omp_get_wtime();
	end_time = end_time - st_time;
	printf("\nTotal Sum = %10d", sum);
	printf("\nTIME OF WORK IS %f \n", end_time);
	return end_time;
}
