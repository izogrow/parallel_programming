#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int reduce_t(int argc, char* argv[])
{

	int TotalSum, ProcSum = 0;
	int ProcRank, ProcNum;
	int N = 1440000;
	//	int N = 16;

	int* x = new int[N];

	srand(0);
	for (int i = 0; i < N; ++i) {
		x[i] = rand() % 14;
	}


	MPI_Status Status;

	srand(0);
	double st_time, end_time;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

	// подготовка данных, рассылка 0-ым процессом по всем остальным
	
	st_time = MPI_Wtime();

	int k = N / ProcNum;			// кол-во элементов на нить
	int i1 = k * ProcRank;			// номер начального элемента для нити
	int i2 = k * (ProcRank + 1);	// номер конечного элемента + 1

	if (ProcRank == ProcNum - 1)
		i2 = N;

	for (int i = i1; i < i2; ++i) {
		ProcSum + x[i];	// Q = 14
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum + x[i];
		ProcSum = ProcSum + x[i];
	}
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Reduce(&ProcSum, &TotalSum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	

	end_time = MPI_Wtime();
	end_time = end_time - st_time;

	if (ProcRank == 0)
	{
		printf("\nTotal Sum = %10d", TotalSum);
		printf("\nTIME OF WORK IS %f ", end_time);
	}

	MPI_Finalize();
	delete x;
	return end_time;
}
