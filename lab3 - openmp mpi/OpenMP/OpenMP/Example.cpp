#define CHUNK 100 
#define NMAX 500000 
#include<omp.h>
#include <stdio.h>
#include <stdlib.h>

int example()
{
    int i;
    //double s = 0;
    int* a = new int[NMAX];
    int* b = new int[NMAX];
    int* sum = new int[NMAX];

    srand(0);
    for (int i = 0; i < NMAX; ++i) {	//заполнение массивов
        a[i] = rand() % 14;
        b[i] = rand() % 14;
    }
    int chunk, n;
    chunk = CHUNK;
    n = NMAX;
    omp_set_num_threads(16);
    
    double st_time, end_time;
    st_time = omp_get_wtime();
#pragma omp parallel for shared (a,b,sum,n) private (i)
    //суммирование векторов
    for (int i = 0; i < n; ++i) {
        sum[i] = a[i] + b[i];
    }
    end_time = omp_get_wtime();
    end_time = end_time - st_time;

    printf("\nTIME OF WORK IS %f ", s);

    return 0;
}