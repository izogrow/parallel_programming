#include <omp.h>
#define CHUNK 100
#define NMAX 100000
#include "stdio.h"
#include <cstdlib>
#include <iostream>
#include "Parallel.h"

using namespace std;

int main(int argc, char* argv[]) {
	hello();
	cout << "atomic:";
	atomic_t();
	cout << "\ncrirical:";
	critical_t();
	cout << "\nreduction:";
	reduction_t();
	cout << "\nnon parallel:";
	non_parallel_t();

}
