#define CHUNK 100
//#define NMAX 1440000
//#define NMAX 16
#include "stdio.h"
#include <cstdlib>
#include <iostream>
#include <chrono> 

using namespace std::chrono;
using namespace std;

int non_parallel_t(int n) {
	int NMAX = n;
	int i;
	//double a[NMAX] = { 1, 2, 3 };
	float* a = new float[NMAX];
	float* b = new float[NMAX];
	float* c = new float[NMAX];

	srand(0);
	for (int i = 0; i < NMAX; ++i) {	//заполнение массива 
		//a[i] = rand() % 14;
		//cout << a[i] << endl;
		a[i] = 1.0f;
		b[i] = 1.0f;
	}

	auto start = high_resolution_clock::now();

	for (i = 0; i < NMAX; i++) {
		c[i] = a[i] + b[i];
	}

	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);
	printf("\nTIME OF WORK IS %f \n", duration);
	return 0;
}
