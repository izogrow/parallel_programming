#pragma once

int atomic_t();
int critical_t();
int reduction_t();
int non_parallel_t();
int hello();