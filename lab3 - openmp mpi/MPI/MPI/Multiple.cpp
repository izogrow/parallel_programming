#include <stdlib.h>
#include "mpi.h"
#include <math.h>
#include <stdio.h>
#define NMAX 14400000

int main1(int argc, char* argv[]) {
    int* a, * b, * c = NULL, s = 0;
    int i, j;
    int* a_loc, * b_loc, * c_loc;
    int ProcRank, ProcNum;

    double st_time, end_time;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
    int count = NMAX / ProcNum;
    // инициализация 0-ым процессом исходных данных
    a = (int*)malloc(sizeof(int) * NMAX);
    b = (int*)malloc(sizeof(int) * NMAX);
    c = (int*)malloc(sizeof(int) * NMAX);
    if (ProcRank == 0) {

        for (i = 0; i < NMAX; i++) {
            a[i] = rand() % 14;
            b[i] = rand() % 14;
        }
    }

    a_loc = (int*)malloc(count * sizeof(int));
    b_loc = (int*)malloc(count * sizeof(int));
    c_loc = (int*)malloc(count * sizeof(int));

    st_time = MPI_Wtime();
    // рассылка 0-ым процессом по всем остальным
    MPI_Scatter(a, count, MPI_INT, a_loc, count, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(b, count, MPI_INT, b_loc, count, MPI_INT, 0, MPI_COMM_WORLD);
    // получение локальной суммы векторов
    for (i = 0; i < count; i++) {
        c_loc[i] = a_loc[i] + b_loc[i];
    }
    // сборка результата 0-ым процессом
    MPI_Gather(c_loc, count, MPI_INT, c, count, MPI_INT, 0, MPI_COMM_WORLD);

    end_time = MPI_Wtime();

    end_time = end_time - st_time;

    if (ProcRank == 0) {
        printf("\nMultiple_N:\n");
        printf("First element = %d ", c[0]);
        printf("Last element = %d ", c[NMAX-1]);
        printf("\nTIME OF WORK IS %f ", end_time);

    }
    free(a);
    free(b);
    free(c);
    free(a_loc);
    free(b_loc);
    free(c_loc);
    MPI_Finalize();
    return 0;
}
