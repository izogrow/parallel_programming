#include <stdlib.h>
#include "mpi.h"
#include <math.h>
#include <stdio.h>
//#define NMAX 14400001
#define NMAX 2400001

int main(int argc, char* argv[]) {
	int* a, * b, * c;
	int* a_loc, * b_loc, * c_loc;
	int s = 0;
	int j, i;
	int ProcRank, ProcNum;

	double start_time, end_time;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

	int k = (NMAX - 1) / ProcNum;
	int* counts = (int*)malloc(ProcNum * sizeof(int));
	int* displs = (int*)malloc(ProcNum * sizeof(int));
	for (i = 0; i < ProcNum - 1; ++i) {
		counts[i] = k;
		displs[i] = k * i;
	}
	counts[ProcNum - 1] = k + 1;
	displs[ProcNum - 1] = k * (ProcNum - 1);
	// инициализация 0-ым процессом исходных данных
	a = (int*)malloc(NMAX * sizeof(int));
	b = (int*)malloc(NMAX * sizeof(int));
	c = (int*)malloc(NMAX * sizeof(int));

	if (ProcRank == 0) {

		for (i = 0; i < NMAX; ++i) {
			a[i] = rand() % 14;
			b[i] = rand() % 14;
		}
	}
	k = counts[ProcRank];
	a_loc = (int*)malloc(k * sizeof(int));
	b_loc = (int*)malloc(k * sizeof(int));
	c_loc = (int*)malloc(k * sizeof(int));

	start_time = MPI_Wtime();

	// рассылка 0-ым процессом по всем остальным
	MPI_Scatterv(a, counts, displs, MPI_INT, a_loc, k, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatterv(b, counts, displs, MPI_INT, b_loc, k, MPI_INT, 0, MPI_COMM_WORLD);

	// получение локальной суммы векторов
	for (i = 0; i < k; ++i) {
		c_loc[i] = a_loc[i] + b_loc[i];
	}

	// сборка результата 0-ым процессом
	MPI_Gatherv(c_loc, k, MPI_INT, c, counts, displs, MPI_INT, 0, MPI_COMM_WORLD);

	end_time = MPI_Wtime();
	end_time = end_time - start_time;


	if (ProcRank == 0) {
		printf("\nNot_multiple_N:\n");
		printf("First element = %d \n", c[0]);
		printf("Last element = %d \n", c[NMAX-1]);
		printf("Time of work is %f \n", end_time);
	}


	free(a);
	free(b);
	free(c);
	free(a_loc);
	free(b_loc);
	free(c_loc);
	MPI_Finalize();
	return 0;
}
